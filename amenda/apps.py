from django.apps import AppConfig


class AmendaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'amenda'
