from django.urls import path

from amenda import views

urlpatterns = [
    path('create-amenda', views.AmendaCreateView.as_view(), name='create_amenda'),
    path('list-amenda', views.AmendaListView.as_view(), name='list_amenda'),
    path('delete-amenda/<int:pk>', views.AmendaDeleteView.as_view(), name='delete_amenda'),
    path('update-amenda/<int:pk>', views.AmendaUpdateView.as_view(), name='update_amenda'),
    path('detail-consult/<int:pk>', views.AmendaDetailView.as_view(), name='detail_amenda'),
]
