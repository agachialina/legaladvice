
from django import forms

from amenda.models import Amenda


class AmendaForm(forms.ModelForm):
    class Meta:
        model = Amenda
        fields = ['situatie_fapt', 'file']
