from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView, DeleteView, DetailView

from amenda.forms import AmendaForm
from amenda.models import Amenda
from user.models import UserExtend


class AmendaCreateView(LoginRequiredMixin, CreateView):
    template_name = 'amenda/create-amenda.html'
    model = Amenda
    reverse_lazy = '/'
    form_class = AmendaForm
    success_url = '/'

    def form_valid(self, form):
        form.instance.extend_user = UserExtend.objects.get(id=self.request.user.id)
        return super().form_valid(form)


class AmendaListView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    template_name = 'amenda/list-amenzi.html'
    model = Amenda
    context_object_name = 'all_amenzi'
    permission_required = 'amenda.view_amenda'


class AmendaUpdateView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    template_name = 'amenda/update_amenda.html'
    model = Amenda
    fields = '__all__'
    success_url = reverse_lazy('list_amenda')
    permission_required = 'amenda.change_amenda'


class AmendaDeleteView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    template_name = 'amenda/delete_amenda.html'
    model = Amenda
    context_object_name = 'amenda'
    success_url = reverse_lazy('list_amenda')
    permission_required = 'amenda.delete_amenda'


class AmendaDetailView(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    template_name = 'amenda/detail_amenda.html'
    model = Amenda
    context_object_name = 'amenda'
    permission_required = 'amenda.view_amenda'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['amenda'] = self.object
        return context






