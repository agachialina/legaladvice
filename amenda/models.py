
from django.db import models
from django.utils import timezone

from user.models import UserExtend


class Amenda(models.Model):
    extend_user = models.ForeignKey(UserExtend, on_delete=models.DO_NOTHING, null=True, blank=True)
    file = models.FileField(upload_to='static/amenzi/')
    created = models.DateTimeField(default=timezone.now)
    situatie_fapt = models.TextField()

    def __str__(self):
        return f'{self.id}'
