from django.db import models


class Despre(models.Model):
    despre = models.TextField()

    def __str__(self):
        return self.despre


class AppDespre(models.Model):
    instructiuni = models.TextField()

    def __str__(self):
        return self.instructiuni
