from django.urls import path

from despre import views

urlpatterns = [
    path('', views.HomeTemplateView.as_view(), name='home'),
    path('create-despre/', views.DespreCreateView.as_view(), name='create-despre'),
    path('list-despre/', views.DespreListView.as_view(), name='list-despre'),
    path('update-despre/<int:pk>/', views.DespreUpdateView.as_view(), name='update-despre'),
    path('create-app/', views.AppCreateView.as_view(), name='create-app'),
    path('despre-app/', views.AppListView.as_view(), name='despre-app'),
    path('update-app/<int:pk>/', views.AppUpdateView.as_view(), name='update-app'),

]

