from django.apps import AppConfig


class DespreConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'despre'
