from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import TemplateView, CreateView, UpdateView, ListView


from despre.forms import DespreForm, AppForm
from despre.models import Despre, AppDespre


class HomeTemplateView(TemplateView):
    template_name = 'home.html'


class DespreCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    template_name = 'despre/create-despre.html'
    model = Despre
    success_url = 'list-despre'
    form_class = DespreForm
    permission_required = 'despre.add_despre'


class DespreListView(ListView):
    template_name = 'despre/list_despre.html'
    model = Despre
    context_object_name = 'all_despre'


class DespreUpdateView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    template_name = 'despre/update_despre.html'
    model = Despre
    fields = '__all__'
    success_url = 'list-despre'
    permission_required = 'despre.change_despre'


class AppCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    template_name = 'despre/app_create.html'
    model = AppDespre
    success_url = reverse_lazy('despre-app')
    form_class = AppForm
    permission_required = 'instructiuni.add_appdespre'


class AppListView(ListView):
    template_name = 'despre/despre_app.html'
    model = AppDespre
    context_object_name = 'all_app'


class AppUpdateView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    template_name = 'despre/update_app.html'
    model = AppDespre
    fields = '__all__'
    success_url = 'despre-app'
    permission_required = 'instructiuni.change_appdespre'
