from django.forms import TextInput
from django import forms
from despre.models import Despre, AppDespre


class DespreForm(forms.ModelForm):
    class Meta:
        model = Despre
        fields = '__all__'
        widgets = {
            'despre': TextInput(attrs={'placeholder': 'Adaugati descrierea: ', 'class': 'form-control'})
        }


class AppForm(forms.ModelForm):
    class Meta:
        model = AppDespre
        fields = '__all__'
        widgets = {
            'instructiuni': TextInput(attrs={'placeholder': 'Adaugati instructiuni: ', 'class': 'form-control'})
        }