
from django.urls import path

from user import views

urlpatterns = [
    path('create-user', views.UserCreateView.as_view(), name='create-user'),
    path('list-users/', views.UserListView.as_view(), name='list_users'),
    path('update-user/<int:pk>', views.UserUpdateView.as_view(), name='update_user'),
    path('detail-user/<int:pk>', views.UserDetailView.as_view(), name='detail_user'),
    path('delete-user/<int:pk>', views.UserDeleteView.as_view(), name='delete_user'),

]