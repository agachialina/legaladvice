# from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.auth.models import Group
from django.shortcuts import redirect

from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView, DeleteView, DetailView

from user.forms import UserExtendForm
from user.models import UserExtend


class UserCreateView(CreateView):
    template_name = 'user/create-user.html'
    form_class = UserExtendForm
    success_url = reverse_lazy('login')

    def form_valid(self, form):
        if self.request.method == 'POST':
            form = UserExtendForm(self.request.POST)
            if form.is_valid():
                user = form.save()
                group = Group.objects.get(name='Clienti')
                user.groups.add(group)
            return redirect('login')
        else:
            form = UserExtendForm()


class UserListView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    template_name = 'user/list_user.html'
    model = UserExtend
    context_object_name = 'all_users'
    permission_required = 'user.view_user'


class UserUpdateView(LoginRequiredMixin, UpdateView):
    template_name = 'user/update_user.html'
    model = UserExtend
    fields = ['last_name', 'first_name', 'email', 'data_nasterii', 'telefon']
    success_url = '/'


class UserDeleteView(LoginRequiredMixin, DeleteView):
    template_name = 'user/delete_user.html'
    model = UserExtend
    context_object_name = 'user'
    success_url = reverse_lazy('list_users')
    permission_required = 'user.delete_user'


class UserDetailView(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    template_name = 'user/detail-user.html'
    model = UserExtend
    context_object_name = 'user'
    permission_required = 'user.view_user'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['user'] = self.object
        return context





















# from user.forms import ConsultatieForm, AmendaForm
# from user.models import Consultatie


# class ConsultCreateView(CreateView):
#     template_name = 'create-user.html'
#     model = Consultatie
#     fields = '__all__'
#     success_url = reverse_lazy('home')
#     # form_class = ConsultatieForm
#
#
# class AmendaCreateView(CreateView):
#     template_name = 'create-user.html'
#     success_url = reverse_lazy('home')
#     form_class = AmendaForm
