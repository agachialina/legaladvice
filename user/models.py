from django.contrib.auth.models import User
from django.db import models


class UserExtend(User):
    nume = models.CharField(max_length=100, null=False, blank=False)
    prenume = models.CharField(max_length=100, null=False, blank=False)
    data_nasterii = models.DateField()
    telefon = models.IntegerField(null=False, blank=False)
    acord_utilizator = models.BooleanField(default=False)


