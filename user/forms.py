from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

from django.forms import TextInput, DateTimeInput, CheckboxInput

from user.models import UserExtend


class UserExtendForm(UserCreationForm):
    class Meta:
        model = UserExtend
        fields = ['username', 'last_name', 'first_name', 'data_nasterii', 'telefon', 'email', 'acord_utilizator']
        labels = {
            'username': 'Numele de utilizator',
            'last_name': 'Nume',
            'first_name': 'Prenume',
            'date_of_birth': 'Data nasterii',
            'email': 'Adresa de email',
            'phone': 'Telefon',
            'is_user_acord': 'Acordul utilizatorului'
        }

        widgets = {
            'username': TextInput(attrs={'placeholder': 'Introduceti numele de utilizator:', 'class': 'form-control'}),
            'last_name': TextInput(attrs={'placeholder': 'Introduceti numele: ', 'class': 'form-control'}),
            'first_name': TextInput(attrs={'placeholder': 'Introduceti prenumele: ', 'class': 'form-control'}),
            'data_nasterii': DateTimeInput(attrs={'placeholder': 'Introduceti data nasterii:',
                                                  'class': 'input', 'type': 'date'}),
            'telefon': TextInput(attrs={'placeholder': 'Introduceti numarul de telefon: ', 'class': 'form-control'}),
            'email': TextInput(attrs={'placeholder': 'Introduceti adresa de email: ', 'class': 'form-control'}),
            'is_user_accord': CheckboxInput(),
        }

    def __init__(self, *args, **kwargs):
        super(UserExtendForm, self).__init__(*args, **kwargs)
        self.fields['password1'].widget.attrs['class'] = 'form-control'
        self.fields['password1'].widget.attrs['placeholder'] = 'Introduceti parola'
        self.fields['password2'].widget.attrs['class'] = 'form-control'
        self.fields['password2'].widget.attrs['placeholder'] = 'Confirmati parola'

    def clean_email(self):
        email = self.cleaned_data.get('email')
        if User.objects.filter(email__iexact=email).exists():
            raise forms.ValidationError("Aceasta adresa de email exista deja in baza de date.")
        return email

    def clean_username(self):
        username = self.cleaned_data.get('username')
        if User.objects.filter(username__iexact=username).exists():
            raise forms.ValidationError("Acest nume de utilizator exista deja in baza de date.")
        return username
