
from django.urls import path

from consultatii_juridice import views

urlpatterns = [
    path('create-consult/', views.ConsultCreateView.as_view(), name='create_consult'),
    path('list-consult/', views.ConsultListView.as_view(), name='list_consult'),
    path('update-consult/<int:pk>', views.ConsultUpdateView.as_view(), name='update_consult'),
    path('detail-consult/<int:pk>', views.ConsultDetailView.as_view(), name='detail_consult'),
    path('delete-consult/<int:pk>', views.ConsultDeleteView.as_view(), name='delete_consult'),


]