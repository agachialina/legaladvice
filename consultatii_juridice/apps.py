from django.apps import AppConfig


class ConsultatiiJuridiceConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'consultatii_juridice'
