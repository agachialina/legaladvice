from django.contrib import admin

from consultatii_juridice.models import Consultatie

admin.site.register(Consultatie)
