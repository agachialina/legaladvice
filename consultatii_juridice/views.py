from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, DetailView, UpdateView, DeleteView

from consultatii_juridice.forms import ConsultForm
from consultatii_juridice.models import Consultatie
from user.models import UserExtend


class ConsultCreateView(LoginRequiredMixin, CreateView):
    template_name = 'consultatie_juridica/create-consult.html'
    model = Consultatie
    form_class = ConsultForm
    success_url = '/'

    def form_valid(self, form):
        form.instance.extend_user = UserExtend.objects.get(id=self.request.user.id)
        return super().form_valid(form)


class ConsultListView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    template_name = 'consultatie_juridica/list-consult.html'
    model = Consultatie
    context_object_name = 'all_consultatii'
    permission_required = 'consultatie.view_consultatie'


class ConsultUpdateView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    template_name = 'consultatie_juridica/update_consult.html'
    model = Consultatie
    fields = '__all__'
    success_url = reverse_lazy('list_consult')
    permission_required = 'consultatie.change_consultatie'


class ConsultDeleteView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    template_name = 'consultatie_juridica/delete_consult.html'
    model = Consultatie
    context_object_name = 'consultatie'
    success_url = reverse_lazy('list_consult')
    permission_required = 'consultatie.delete_consultatie'


class ConsultDetailView(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    template_name = 'consultatie_juridica/detail-consult.html'
    model = Consultatie
    context_object_name = 'consultatie'
    permission_required = 'consultatie.view_consultatie'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['consultatie'] = self.object
        return context
