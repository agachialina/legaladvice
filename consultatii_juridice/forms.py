
from django import forms


from consultatii_juridice.models import Consultatie


class ConsultForm(forms.ModelForm):
    class Meta:
        model = Consultatie
        fields = ['situatie_fapt', 'file']

