from django.db import models
from django.utils import timezone

from user.models import UserExtend


class Consultatie(models.Model):
    extend_user = models.ForeignKey(UserExtend, on_delete=models.DO_NOTHING, null=True, blank=True,
                                    related_name='extend_user')

    file = models.FileField(upload_to='static/consultatii/')
    created = models.DateTimeField(default=timezone.now)
    situatie_fapt = models.TextField()

    def __str__(self):
        return f'{self.id}'

    # def save(self, request=None, *args, **kwargs):
    #     print(f'{args}')
    #     print('saving consultatie')
    #     if request:
    #         print(f'Consultatie belongs to: {request.user.get_full_name()}')
    #         self.extend_user = UserExtend.objects.get(id=request.user.id)
    #     super().save()