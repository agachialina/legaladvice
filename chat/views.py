from django.shortcuts import render, redirect

from django.urls import reverse_lazy

from chat.models import Message
from user.models import UserExtend


class UserWithMessage:
    def __init__(self, user, message):
        self.user: UserExtend = user
        self.message: Message = message

    def __eq__(self, other):
        return self.user.__eq__(other.user)

    def __repr__(self):
        return f'{self.user.username}: {self.message.message[:30]}'


def chat_view(request, user_id=None):
    current_user: UserExtend = UserExtend.objects.get(id=request.user.id)
    lawyer = UserExtend.objects.get(is_superuser=True)

    # if normal user asks for specific conversation, redirect to normal chat, with no user_id
    if user_id is not None and not current_user.is_superuser:
        return redirect(reverse_lazy('chat'))

    # if it's superadmin, redirect to first conversation
    # if user_id is None and current_user.is_superuser:
    #     return redirect(reverse_lazy('chat', {'user_id':}))

    # if there is a superadmin, they request specific user_id for conversations
    if user_id is not None and current_user.is_superuser:
        conversation_partner = UserExtend.objects.get(id=user_id)
    else:
        conversation_partner = lawyer

    if request.method == 'POST':
        message = request.POST.get('message', '')
        to_user_id = request.POST.get('to_user_id')
        file = request.FILES.get('attach')
        to_user = UserExtend.objects.get(id=to_user_id)
        Message.objects.create(sender=current_user, receiver=to_user, message=message, attach=file)

    users_with_last_messages = []

    if current_user.is_superuser:

        all_messages = Message.objects.order_by('-timestamp')
        print(all_messages)
        for message in all_messages:
            user_with_message = UserWithMessage(
                message.sender if message.receiver == current_user else message.receiver,
                message)
            if user_with_message not in users_with_last_messages:
                users_with_last_messages.append(user_with_message)
        print(users_with_last_messages)

        existing_users = map(lambda user_with_message: user_with_message.user, users_with_last_messages)
        all_users = UserExtend.objects.all()

        e_users_set = set(existing_users)
        a_users_set = set(all_users)
        m_users_set = a_users_set.difference(e_users_set)
        missing_users = list(m_users_set)
        missing_users.remove(current_user)
        missing_users_with_last_messages = map(lambda user: UserWithMessage(user, 'Start a conversation now!'),
                                               missing_users)
        users_with_last_messages.extend(missing_users_with_last_messages)

        conversation = get_conversation(current_user.id, conversation_partner.id)
        first = UserWithMessage(conversation_partner, get_last_message(lawyer.id, conversation_partner.id))

    else:
        conversation = get_conversation(lawyer.id, current_user.id)
        if len(conversation) > 0:
            first = UserWithMessage(lawyer, get_last_message(lawyer.id, current_user.id))
            users_with_last_messages.append(first)
        else:
            first = None

    return render(request, 'chat.html', {'users_with_last_messages': users_with_last_messages, 'selected': first,
                                         'conversation': conversation, 'user': current_user})


def get_conversation(sender_id, receiver_id):
    messages_from = Message.objects.filter(sender_id=sender_id, receiver_id=receiver_id).order_by('-timestamp')
    messages_to = Message.objects.filter(receiver=sender_id, sender_id=receiver_id).order_by('-timestamp')
    conversation = set(messages_from).union(set(messages_to))  # make items unique
    return list(conversation)


def get_last_message(sender_id, receiver_id):
    conversation = get_conversation(sender_id=sender_id, receiver_id=receiver_id)
    last_message = conversation[0] if len(conversation) > 0 else f'Write a message to allow this user to respond'
    return last_message


#
#
#
#

