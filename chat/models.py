from django.contrib.auth.models import User
from django.db import models

from user.models import UserExtend


class Message(models.Model):
    sender = models.ForeignKey(UserExtend, on_delete=models.CASCADE, related_name='sender')
    receiver = models.ForeignKey(UserExtend, on_delete=models.CASCADE, related_name='receiver')
    message = models.CharField(max_length=1200)
    timestamp = models.DateTimeField(auto_now_add=True)
    is_read = models.BooleanField(default=False)
    attach = models.FileField(upload_to='static/chat/', null=True, blank=True)

    def __str__(self):
        return f'{self.sender.get_full_name()}->{self.receiver.get_full_name()}: {self.message}'

    class Meta:
        ordering = ('timestamp',)
