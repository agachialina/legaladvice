from django.urls import path
from chat import views

urlpatterns = [
    # path('chat', views.ChatView.as_view(), name='chat')
    # URL form : "/api/messages/1/2"
    # path('api/messages/<int:sender>/<int:receiver>', views.message_list, name='message-detail'),  # For GET request.
    # URL form : "/api/messages/"
    # path('api/messages/', views.message_list, name='message-list'),  # For POST
    # URL form "/api/users/1"
    # path('api/users/<int:pk>', views.user_list, name='user-detail'),  # GET request for user with id
    path('chat/', views.chat_view, name='chat'),# POST for new user and GET for all users list
    path('chat/<int:user_id>/', views.chat_view, name='chat_details')

]
