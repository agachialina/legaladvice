from django.apps import AppConfig


class InfoVideoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'info_video'
