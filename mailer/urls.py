
from django.urls import path

from mailer.views import send_email

urlpatterns = [
    path('email/', send_email, name='send_email'),
]
