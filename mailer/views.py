from django.conf import settings
from django.core.mail import send_mail
from django.shortcuts import render

from mailer.forms import EmailForm


def send_email(request):
    message_sent = False
    if request.method == 'POST':
        form = EmailForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            subject = 'Raspuns Legal Advice'
            message = cd['message']
            send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, [cd['recipient']])
            message_sent = True
    else:
        form = EmailForm()
    return render(request, 'index.html', {
        'form': form,
        'message_sent': message_sent
    })
