from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.shortcuts import render
from django.urls import reverse_lazy
from django.utils import timezone
from django.views.generic import CreateView, ListView, UpdateView, DeleteView
import calendar
import datetime
import time

from programari.forms import AppointmentsForm
from programari.models import Appointments


class AppointmentsCreateView(LoginRequiredMixin, CreateView):
    template_name = 'programari/create_appointment.html'
    model = Appointments
    success_url = '/'
    form_class = AppointmentsForm


class AppointmentListView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    template_name = 'programari/list_appointment.html'
    model = Appointments
    context_object_name = 'all_programari'
    permission_required = 'programari.view_appointments'


class AppointmentUpdateView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    template_name = 'programari/update_appointment.html'
    model = Appointments
    fields = '__all__'
    success_url = reverse_lazy('list_appointment')
    permission_required = 'programari.change_appointments'


class AppointmentDeleteView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    template_name = 'programari/delete_appointment.html'
    model = Appointments
    context_object_name = 'programare'
    success_url = reverse_lazy('list_appointment')
    permission_required = 'programari.delete_appointments'


def calendar_view(request):
    if request.method == 'GET':
        print(request.GET)
        weeks_from_now = request.GET.get('weeks_from_now', 0)
        if weeks_from_now == '':
            weeks_from_now = 0
        if isinstance(weeks_from_now, str):
            weeks_from_now = int(weeks_from_now)
        week_button = request.GET.get('week')
        if week_button == 'next':
            weeks_from_now = weeks_from_now + 1
        elif week_button == 'previous':
            weeks_from_now = weeks_from_now - 1
    if weeks_from_now is None:
        weeks_from_now = 0

    all_programari = Appointments.objects.all()
    context = dict()
    context['all_programari '] = all_programari
    context['weeks_from_now'] = weeks_from_now
    context['max_weeks_from_now'] = 4
    context['schedule'] = get_schedule(weeks_from_now=weeks_from_now)
    # context['days'] = get_current_week(weeks_to_add=weeks_from_now)
    # context['hours'] = get_hours()
    return render(request, 'programari/calendar.html', context)


# class ToDoListCalendarView(ListView):
#     template_name = 'to_do_list/list_events_calendar.html'
#     model = ToDo
#     context_object_name = 'all_events'
#
#     def get_context_data(self, *, object_list=None, **kwargs):
#         context = super(ToDoListCalendarView, self).get_context_data()
#         context['days'] = get_current_week()
#         context['hours'] = get_hours()
#         return context


def get_current_week(weeks_to_add=0):
    now = timezone.now()
    # now = now + datetime.timedelta(days=24)  #helper pentru test
    print(now.weekday())
    while now.weekday() != 0:
        now = now - datetime.timedelta(days=1)
    days = []
    now = now + datetime.timedelta(weeks=weeks_to_add)
    for i in range(7):
        days.append(
            {'day': now.day, 'month': calendar.month_abbr[now.month], 'work_day': calendar.day_abbr[now.weekday()]})

        now = now + datetime.timedelta(days=1)
    return days


def get_hours(start_hour=9, start_minute=0, end_hour=17, end_minute=0, minutes_step=30):
    times = []
    now = timezone.now()
    hour_difference = now.hour - start_hour
    minute_difference = now.minute - start_minute
    now = now - datetime.timedelta(hours=hour_difference, minutes=minute_difference)
    print(hour_difference)
    print(minute_difference)
    print(now.hour)
    print(now.minute)

    while not (now.hour == end_hour and now.minute == end_minute):
        times.append({'time': now.strftime("%H:%M")})
        now = now + datetime.timedelta(minutes=minutes_step)
    return times


def get_schedule(weeks_from_now=0):
    rows = []
    first_row = []
    for day in get_current_week(weeks_from_now):
        first_row.append(day)
    rows.append(first_row)
    for hour in get_hours():
        row = list()
        row.append(hour)
        for day in get_current_week(weeks_from_now):
            # TODO continue here
            row.append({'status': 'free' if time.time() % 2 == 0 else 'busy'})
        rows.append(row)
    return rows
