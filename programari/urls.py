from django.urls import path

from programari import views

urlpatterns = [
    path('create_appointment/', views.AppointmentsCreateView.as_view(), name='create_appointment'),
    path('list_appointment/', views.AppointmentListView.as_view(), name='list_appointment'),
    path('update_appointment/<int:pk>', views.AppointmentUpdateView.as_view(), name='update_appointment'),
    path('delete_appointment/<int:pk>', views.AppointmentDeleteView.as_view(), name='delete_appointment'),
    path('calendar', views.calendar_view, name='calendar'),
]
