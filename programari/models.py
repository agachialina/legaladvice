from django.db import models


class Appointments(models.Model):
    title = models.CharField(max_length=150, null=False, blank=False)
    description = models.TextField()
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()
