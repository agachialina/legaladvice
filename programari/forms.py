from django import forms
from django.forms import TextInput, DateTimeInput, DateTimeField

from programari.models import Appointments


class AppointmentsForm(forms.ModelForm):
    class Meta:
        model = Appointments
        fields = ['title', 'description', 'start_date', 'end_date']
        widgets = {
            'title': TextInput(attrs={'placeholder': 'Introduceți titlul programării', 'class': 'form-control'}),
            'description': TextInput(attrs={'placeholder': 'Introduceți descrierea programării', 'class': 'form-control'}),
            # 'created': DateTimeField(),
            'start_date': DateTimeInput(attrs={'placeholder': 'Introduceți data si ora la care începe programarea', 'class': 'form-control', 'type': 'datetime-local'}),
            'end_date': DateTimeInput(attrs={'placeholder': 'Introduceți data și ora la care se termină programarea', 'class': 'form-control', 'type': 'datetime-local'}),
        }
